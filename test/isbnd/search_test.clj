(ns isbnd.search-test
  (:require [isbnd.search :as sut]
            [clojure.test :as t :refer [deftest run-tests]]
            [cheshire.core :refer [generate-string]])
  (:use clj-http.fake))

(def testbook1 (generate-string {
                                 :kind "books#volumes",
                                 :totalItems 1,
                                 :items [
                                         {
                                          :kind "books#volume",
                                          :id "xColAAPGubgC",
                                          :etag "tvZ33cRv8Rw",
                                          :selfLink "https://www.googleapis.com/books/v1/volumes/xColAAPGubgC",
                                          :volumeInfo {
                                                       :title "Domain-driven Design",
                                                       :subtitle "Tackling Complexity in the Heart of Software",
                                                       :authors [
                                                                 "Eric Evans"
                                                                 ],
                                                       :publisher "Addison-Wesley Professional",
                                                       :publishDate "2004",
                                                       :description "Describes ways to incorporate domain modeling into software development.",
                                                       "industryIdentifiers": [
                                                                                   {
                                                                                    "type": "ISBN_13",
                                                                                    "identifier": "9780321125217"
                                                                                    },
                                                                                   {
                                                                                    "type": "ISBN_10",
                                                                                    "identifier": "0321125215"
                                                                                    }
                                                                                   ],
                                                           "readingModes": {
                                                                            "text": false,
                                                                            "image": true
                                                                            },
                                                           "pageCount": 529,
                                                           "printType": "BOOK",
                                                           "categories": [
                                                                          "Computers"
                                                                          ],
                                                           "averageRating": 5.0,
                                                           "ratingsCount": 4,
                                                           "maturityRating": "NOT_MATURE",
                                                           "allowAnonLogging": false,
                                                           "contentVersion": "preview-1.0.0",
                                                           "imageLinks": {
                                                                          "smallThumbnail": "http://books.google.com/books/content?id=xColAAPGubgC&printsec=frontcover&img=1&zoom=5&edge=curl&source=gbs_api",
                                                                          "thumbnail": "http://books.google.com/books/content?id=xColAAPGubgC&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api"
                                                                          },
                                                           "language": "en",
                                                           "previewLink": "http://books.google.at/books?id=xColAAPGubgC&printsec=frontcover&dq=isbn:9780321125217&hl=&cd=1&source=gbs_api",
                                                           "infoLink": "http://books.google.at/books?id=xColAAPGubgC&dq=isbn:9780321125217&hl=&source=gbs_api",
                                                           "canonicalVolumeLink": "https://books.google.com/books/about/Domain_driven_Design.html?hl=&id=xColAAPGubgC"
                                                           },
                                            "saleInfo": {
                                                         "country": "AT",
                                                         "saleability": "NOT_FOR_SALE",
                                                         "isEbook": false
                                                         },
                                            "accessInfo": {
                                                           "country": "AT",
                                                           "viewability": "PARTIAL",
                                                           "embeddable": true,
                                                           "publicDomain": false,
                                                           "textToSpeechPermission": "ALLOWED_FOR_ACCESSIBILITY",
                                                           "epub": {
                                                                    "isAvailable": false
                                                                    },
                                                           "pdf": {
                                                                   "isAvailable": false
                                                                   },
                                                           "webReaderLink": "http://play.google.com/books/reader?id=xColAAPGubgC&hl=&printsec=frontcover&source=gbs_api",
                                                           "accessViewStatus": "SAMPLE",
                                                           "quoteSharingAllowed": false
                                                           },
                                            "searchInfo": {
                                                           "textSnippet": "Describes ways to incorporate domain modeling into software development."
                                                           }
                                            }
                                           ]
                                 }))

(deftest testQueryISBN
  (with-global-fake-routes {
                     #"https://www.googleapis.com/.*" (fn [request] {:status 200 :body "hello world" })
                     }
    (let [response (sut/isbn? "9780321125217")]
      (prn response))
    ))
