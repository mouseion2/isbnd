(ns isbnd.search
  (:require [isbnd.http :as http]))

(def booksapiURL "https://www.googleapis.com/books/v1/volumes")

(defn convert [info]
  (prn info)
  {:kind "bookinformation"
   :version "v1"
   :data {:title (:title info)
          }
   })

(defn isbn? [isbn]
  "Returns information for the given ISBN in the form:

kind: bookinformation
version: v1
data:
  title: the title
  subtitle: the sub title
  authors:
    - author 1
    - author n
  publisher: the publisher
  publishDate: 1.1.1970
  description: the description
  isbn13: 1234567890123
  isbn10: 1234567890
  thumbnail: http://link.to/thumbnail.jpg"
  (let [query (str booksapiURL "?q=isnb:" isbn)
        info (http/fetch query)]
    (convert info)))
