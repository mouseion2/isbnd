(ns isbnd.http
  (:require [clj-http.client :as client]
            [clojure.tools.logging :as log]))

(defn fetch [url]
  (log/info "Requesting url" url)
  (try
    (let [jsondata (client/get url {:accept :json
                                    :as :json
                                    :socket-timeout 1000
                                    :connection-timeout 1000})]
      (:body jsondata))
    (catch Exception e (log/warn "caught exception:" (.toString e)))))
